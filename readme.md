# Projet blog - Cours Dev Back

#### **Lien du projet : https://gitlab.com/EloxFire/blog_project_avagliano_symfony**




### Utilisation du projet :

- Cloner le répertoire Git :

  ```bash
  git clone https://gitlab.com/EloxFire/blog_project_avagliano_symfony
  ```

- Installer les dépendances

  ```bash
  cd blog_project_avagliano_symfony # Naviger dans le dossier du projet
  
  composer install # Installer composer
  
  php bin/console doctrine:database:create # Creer la base de données
  
  php bin/console doctrine:fixture:load # Charger les utilisateurs par défaut
  ```

- Lancer le serveur

  ```bash
  symfony server:start # Démarrer le serveur symfony
  ```



### Spécificités du projet :

- **Autorisations :**
  - **Accueil (/)** : Visible par tout le monde
  - **A propos (/a-propos)** : Visible par tout le monde
  - **Articles (/articles/article-table)** :
    - Articles Public = Oui : Visible par tout le monde (même pas connectés)
    - Articles Public = Non : Visibles uniquement quand utilisateur connecté
  - **Contact (/contact/new)** : Visible par tout le monde
  - **Inscription (/user/new)** : Visible par tout le monde
  - **Connexion (/login)** : Visible par tout le monde
  - **Mes paramètres (/user/:id/edit)** : Visible par l'utilisateur connecté uniquement
  - **Créer un article (/article/new)** : Visible par les utilisateurs administrateurs uniquement



- **Utilisateurs disponibles par défaut :**

  - | Mail           | Mot de passe     | Role           |
    | -------------- | ---------------- | -------------- |
    | enzo@test.com  | enzopass         | User           |
    | admin@test.com | unMot2PasseAdm/n | Administrateur |



- Schéma de la base de donnée fonctionnelle :

  ![](https://media.discordapp.net/attachments/814250385190420483/833722063624863844/blog_diagram_db.png)

