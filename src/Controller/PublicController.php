<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class PublicController extends AbstractController
{
    /**
     * @Route("/a-propos", name="about")
     */
    public function index(): Response
    {
        return $this->render('about.html.twig', [
            'controller_name' => 'PublicController',
        ]);
    }
}
