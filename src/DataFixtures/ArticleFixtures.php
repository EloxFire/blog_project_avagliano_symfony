<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use App\Entity\Article;

class ArticleFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $article1 = new Article();
        $article1->setTitle("Lorem Ipsum")
        ->setSubtitle("Lorem ipsum dolor sit amet")
        ->setContent("Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce blandit massa eget lectus venenatis, in interdum massa luctus. Suspendisse potenti. Maecenas vitae magna turpis. Nullam in convallis dolor. Sed turpis leo, euismod in mi at, interdum ornare lorem. Suspendisse nec nunc sagittis, scelerisque libero nec, molestie lorem.")
        ->setImage("https://i.picsum.photos/id/663/1920/1080.jpg?hmac=oCL_DUl5RW2SZrDG1p94fRkWpBBCL1JJKwXC_pb7Jr8")
        ->setDate(date('Y-m-d'))
        ->setIsPublic(1);

        $article2 = new Article();
        $article2->setTitle("Lorem Ipsum")
        ->setSubtitle("Lorem ipsum dolor sit amet")
        ->setContent("Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce blandit massa eget lectus venenatis, in interdum massa luctus. Suspendisse potenti. Maecenas vitae magna turpis. Nullam in convallis dolor. Sed turpis leo, euismod in mi at, interdum ornare lorem. Suspendisse nec nunc sagittis, scelerisque libero nec, molestie lorem.")
        ->setImage("https://i.picsum.photos/id/702/1920/1080.jpg?hmac=n6eP3W7CD344wRBSdxaVca7lgJSL2J5N7rll03ILCg0")
        ->setDate(date('Y-m-d'))
        ->setIsPublic(0);

        $article3 = new Article();
        $article3->setTitle("Lorem Ipsum")
        ->setSubtitle("Lorem ipsum dolor sit amet")
        ->setContent("Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce blandit massa eget lectus venenatis, in interdum massa luctus. Suspendisse potenti. Maecenas vitae magna turpis. Nullam in convallis dolor. Sed turpis leo, euismod in mi at, interdum ornare lorem. Suspendisse nec nunc sagittis, scelerisque libero nec, molestie lorem.")
        ->setImage("https://i.picsum.photos/id/663/1920/1080.jpg?hmac=oCL_DUl5RW2SZrDG1p94fRkWpBBCL1JJKwXC_pb7Jr8")
        ->setDate(date('Y-m-d'))
        ->setIsPublic(1);

        $article4 = new Article();
        $article4->setTitle("Lorem Ipsum")
        ->setSubtitle("Lorem ipsum dolor sit amet")
        ->setContent("Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce blandit massa eget lectus venenatis, in interdum massa luctus. Suspendisse potenti. Maecenas vitae magna turpis. Nullam in convallis dolor. Sed turpis leo, euismod in mi at, interdum ornare lorem. Suspendisse nec nunc sagittis, scelerisque libero nec, molestie lorem.")
        ->setImage("https://i.picsum.photos/id/764/1920/1080.jpg?hmac=hG5nY3fs7ire3rcwVWMw4tcgYnPew0b1raqMRAy7xZ8")
        ->setDate(date('Y-m-d'))
        ->setIsPublic(1);

        $article5 = new Article();
        $article5->setTitle("Lorem Ipsum")
        ->setSubtitle("Lorem ipsum dolor sit amet")
        ->setContent("Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce blandit massa eget lectus venenatis, in interdum massa luctus. Suspendisse potenti. Maecenas vitae magna turpis. Nullam in convallis dolor. Sed turpis leo, euismod in mi at, interdum ornare lorem. Suspendisse nec nunc sagittis, scelerisque libero nec, molestie lorem.")
        ->setImage("https://i.picsum.photos/id/737/1920/1080.jpg?hmac=aFzER8Y4wcWTrXVx2wVKSj10IqnygaF33gESj0WGDwI")
        ->setDate(date('Y-m-d'))
        ->setIsPublic(0);

        $article6 = new Article();
        $article6->setTitle("Lorem Ipsum")
        ->setSubtitle("Lorem ipsum dolor sit amet")
        ->setContent("Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce blandit massa eget lectus venenatis, in interdum massa luctus. Suspendisse potenti. Maecenas vitae magna turpis. Nullam in convallis dolor. Sed turpis leo, euismod in mi at, interdum ornare lorem. Suspendisse nec nunc sagittis, scelerisque libero nec, molestie lorem.")
        ->setImage("https://i.picsum.photos/id/300/1920/1080.jpg?hmac=oo6UaBZDXLQWMeRFA7TGRlx1H7A8yD9ZHVtq7s4DXXM")
        ->setDate(date('Y-m-d'))
        ->setIsPublic(0);

        $article7 = new Article();
        $article7->setTitle("Lorem Ipsum")
        ->setSubtitle("Lorem ipsum dolor sit amet")
        ->setContent("Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce blandit massa eget lectus venenatis, in interdum massa luctus. Suspendisse potenti. Maecenas vitae magna turpis. Nullam in convallis dolor. Sed turpis leo, euismod in mi at, interdum ornare lorem. Suspendisse nec nunc sagittis, scelerisque libero nec, molestie lorem.")
        ->setImage("https://i.picsum.photos/id/613/1920/1080.jpg?hmac=9a2rU74zXbNhsr6FLLb6RuWbM7VhxiryC6SSugxDidw")
        ->setDate(date('Y-m-d'))
        ->setIsPublic(1);

        $article8 = new Article();
        $article8->setTitle("Lorem Ipsum")
        ->setSubtitle("Lorem ipsum dolor sit amet")
        ->setContent("Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce blandit massa eget lectus venenatis, in interdum massa luctus. Suspendisse potenti. Maecenas vitae magna turpis. Nullam in convallis dolor. Sed turpis leo, euismod in mi at, interdum ornare lorem. Suspendisse nec nunc sagittis, scelerisque libero nec, molestie lorem.")
        ->setImage("https://i.picsum.photos/id/316/1920/1080.jpg?hmac=ePKjLT9NShF6345zDYLbv-XLsJCQdGDzDv9JvWaVxmw")
        ->setDate(date('Y-m-d'))
        ->setIsPublic(0);

        $article9 = new Article();
        $article9->setTitle("Lorem Ipsum")
        ->setSubtitle("Lorem ipsum dolor sit amet")
        ->setContent("Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce blandit massa eget lectus venenatis, in interdum massa luctus. Suspendisse potenti. Maecenas vitae magna turpis. Nullam in convallis dolor. Sed turpis leo, euismod in mi at, interdum ornare lorem. Suspendisse nec nunc sagittis, scelerisque libero nec, molestie lorem.")
        ->setImage("https://i.picsum.photos/id/1032/1920/1080.jpg?hmac=7wpVjpyV-lhmJZlnDWBHdkZpi6cZe52ixlp93aeB-Zo")
        ->setDate(date('Y-m-d'))
        ->setIsPublic(1);

        $article10 = new Article();
        $article10->setTitle("Lorem Ipsum")
        ->setSubtitle("Lorem ipsum dolor sit amet")
        ->setContent("Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce blandit massa eget lectus venenatis, in interdum massa luctus. Suspendisse potenti. Maecenas vitae magna turpis. Nullam in convallis dolor. Sed turpis leo, euismod in mi at, interdum ornare lorem. Suspendisse nec nunc sagittis, scelerisque libero nec, molestie lorem.")
        ->setImage("https://i.picsum.photos/id/504/1920/1080.jpg?hmac=ttR-2tb1OCS5uHUZZyxY_JWh-0dUUKdDPTuuG9k3bUg")
        ->setDate(date('Y-m-d'))
        ->setIsPublic(1);

        $article11 = new Article();
        $article11->setTitle("Lorem Ipsum")
        ->setSubtitle("Lorem ipsum dolor sit amet")
        ->setContent("Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce blandit massa eget lectus venenatis, in interdum massa luctus. Suspendisse potenti. Maecenas vitae magna turpis. Nullam in convallis dolor. Sed turpis leo, euismod in mi at, interdum ornare lorem. Suspendisse nec nunc sagittis, scelerisque libero nec, molestie lorem.")
        ->setImage("https://i.picsum.photos/id/51/1920/1080.jpg?hmac=PGvMkd3UH8kV0sAgoRoQgA3nvRJQyQb7hGlYwidsKpk")
        ->setDate(date('Y-m-d'))
        ->setIsPublic(0);

        $article12 = new Article();
        $article12->setTitle("Lorem Ipsum")
        ->setSubtitle("Lorem ipsum dolor sit amet")
        ->setContent("Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce blandit massa eget lectus venenatis, in interdum massa luctus. Suspendisse potenti. Maecenas vitae magna turpis. Nullam in convallis dolor. Sed turpis leo, euismod in mi at, interdum ornare lorem. Suspendisse nec nunc sagittis, scelerisque libero nec, molestie lorem.")
        ->setImage("https://i.picsum.photos/id/478/1920/1080.jpg?hmac=z_P2Q370RKvoPEg8xpk6Uhun1h9Z62las4l_2LMoE1Q")
        ->setDate(date('Y-m-d'))
        ->setIsPublic(1);

        $article13 = new Article();
        $article13->setTitle("Lorem Ipsum")
        ->setSubtitle("Lorem ipsum dolor sit amet")
        ->setContent("Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce blandit massa eget lectus venenatis, in interdum massa luctus. Suspendisse potenti. Maecenas vitae magna turpis. Nullam in convallis dolor. Sed turpis leo, euismod in mi at, interdum ornare lorem. Suspendisse nec nunc sagittis, scelerisque libero nec, molestie lorem.")
        ->setImage("https://i.picsum.photos/id/803/1920/1080.jpg?hmac=-gM5an51BXrzFZbCedKzh_fSBPORf0P_M-mSQURmfhA")
        ->setDate(date('Y-m-d'))
        ->setIsPublic(1);

        $article14 = new Article();
        $article14->setTitle("Lorem Ipsum")
        ->setSubtitle("Lorem ipsum dolor sit amet")
        ->setContent("Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce blandit massa eget lectus venenatis, in interdum massa luctus. Suspendisse potenti. Maecenas vitae magna turpis. Nullam in convallis dolor. Sed turpis leo, euismod in mi at, interdum ornare lorem. Suspendisse nec nunc sagittis, scelerisque libero nec, molestie lorem.")
        ->setImage("https://i.picsum.photos/id/663/1920/1080.jpg?hmac=oCL_DUl5RW2SZrDG1p94fRkWpBBCL1JJKwXC_pb7Jr8")
        ->setDate(date('Y-m-d'))
        ->setIsPublic(0);

        $article15 = new Article();
        $article15->setTitle("Lorem Ipsum")
        ->setSubtitle("Lorem ipsum dolor sit amet")
        ->setContent("Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce blandit massa eget lectus venenatis, in interdum massa luctus. Suspendisse potenti. Maecenas vitae magna turpis. Nullam in convallis dolor. Sed turpis leo, euismod in mi at, interdum ornare lorem. Suspendisse nec nunc sagittis, scelerisque libero nec, molestie lorem.")
        ->setImage("https://i.picsum.photos/id/821/1920/1080.jpg?hmac=k7CcyjvnVJYtGyvvHOZwo8mMJn4btCMhJiQQuu9QHfY")
        ->setDate(date('Y-m-d'))
        ->setIsPublic(0);

        $article16 = new Article();
        $article16->setTitle("Lorem Ipsum")
        ->setSubtitle("Lorem ipsum dolor sit amet")
        ->setContent("Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce blandit massa eget lectus venenatis, in interdum massa luctus. Suspendisse potenti. Maecenas vitae magna turpis. Nullam in convallis dolor. Sed turpis leo, euismod in mi at, interdum ornare lorem. Suspendisse nec nunc sagittis, scelerisque libero nec, molestie lorem.")
        ->setImage("https://i.picsum.photos/id/663/1920/1080.jpg?hmac=oCL_DUl5RW2SZrDG1p94fRkWpBBCL1JJKwXC_pb7Jr8")
        ->setDate(date('Y-m-d'))
        ->setIsPublic(1);

        $article17 = new Article();
        $article17->setTitle("Lorem Ipsum")
        ->setSubtitle("Lorem ipsum dolor sit amet")
        ->setContent("Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce blandit massa eget lectus venenatis, in interdum massa luctus. Suspendisse potenti. Maecenas vitae magna turpis. Nullam in convallis dolor. Sed turpis leo, euismod in mi at, interdum ornare lorem. Suspendisse nec nunc sagittis, scelerisque libero nec, molestie lorem.")
        ->setImage("https://i.picsum.photos/id/663/1920/1080.jpg?hmac=oCL_DUl5RW2SZrDG1p94fRkWpBBCL1JJKwXC_pb7Jr8")
        ->setDate(date('Y-m-d'))
        ->setIsPublic(1);

        $article18 = new Article();
        $article18->setTitle("Lorem Ipsum")
        ->setSubtitle("Lorem ipsum dolor sit amet")
        ->setContent("Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce blandit massa eget lectus venenatis, in interdum massa luctus. Suspendisse potenti. Maecenas vitae magna turpis. Nullam in convallis dolor. Sed turpis leo, euismod in mi at, interdum ornare lorem. Suspendisse nec nunc sagittis, scelerisque libero nec, molestie lorem.")
        ->setImage("https://i.picsum.photos/id/232/1920/1080.jpg?hmac=9ep4R7HwKsrH1LbkZqyycvWWmRb2bvayRFCUHTa_NJc")
        ->setDate(date('Y-m-d'))
        ->setIsPublic(1);

        $article19 = new Article();
        $article19->setTitle("Lorem Ipsum")
        ->setSubtitle("Lorem ipsum dolor sit amet")
        ->setContent("Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce blandit massa eget lectus venenatis, in interdum massa luctus. Suspendisse potenti. Maecenas vitae magna turpis. Nullam in convallis dolor. Sed turpis leo, euismod in mi at, interdum ornare lorem. Suspendisse nec nunc sagittis, scelerisque libero nec, molestie lorem.")
        ->setImage("https://i.picsum.photos/id/458/1920/1080.jpg?hmac=gBg5TP6UiTUCfocuYNUw6BCZRq0_kK5EQRSc5Ei-2kg")
        ->setDate(date('Y-m-d'))
        ->setIsPublic(1);

        $article20 = new Article();
        $article20->setTitle("Lorem Ipsum")
        ->setSubtitle("Lorem ipsum dolor sit amet")
        ->setContent("Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce blandit massa eget lectus venenatis, in interdum massa luctus. Suspendisse potenti. Maecenas vitae magna turpis. Nullam in convallis dolor. Sed turpis leo, euismod in mi at, interdum ornare lorem. Suspendisse nec nunc sagittis, scelerisque libero nec, molestie lorem.")
        ->setImage("https://i.picsum.photos/id/1032/1920/1080.jpg?hmac=7wpVjpyV-lhmJZlnDWBHdkZpi6cZe52ixlp93aeB-Zo")
        ->setDate(date('Y-m-d'))
        ->setIsPublic(0);

        $manager->persist($article1);
        $manager->persist($article2);
        $manager->persist($article3);
        $manager->persist($article4);
        $manager->persist($article5);
        $manager->persist($article6);
        $manager->persist($article7);
        $manager->persist($article8);
        $manager->persist($article9);
        $manager->persist($article10);
        $manager->persist($article11);
        $manager->persist($article12);
        $manager->persist($article13);
        $manager->persist($article14);
        $manager->persist($article15);
        $manager->persist($article16);
        $manager->persist($article17);
        $manager->persist($article18);
        $manager->persist($article19);
        $manager->persist($article20);

        $manager->flush();
    }
}
