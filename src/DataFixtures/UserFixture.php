<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use App\Entity\User;

class UserFixture extends Fixture
{

  public function __construct(UserPasswordEncoderInterface $encoder)
  {
    $this->encoder = $encoder;
  }

  public function load(ObjectManager $manager)
  {
    $user = new User();
    $user->setEmail('enzo@test.com')
    ->setPassword($this->encoder->encodePassword($user, 'enzopass'))
    ->setFirstname('Enzo')
    ->setLastName('Avagliano')
    ->setRoles(['ROLE_USER']);

    $manager->persist($user);

    $admin = new User();
    $admin->setEmail('admin@test.com')
    ->setPassword($this->encoder->encodePassword($admin, 'unMot2PasseAdm/n'))
    ->setFirstname('Administrateur')
    ->setLastName('Blog')
    ->setRoles(['ROLE_ADMIN']);

    $manager->persist($admin);

    $manager->flush();
  }
}
